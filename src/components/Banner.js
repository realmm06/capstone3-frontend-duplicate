import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import imgCoffeeCup from '../images/Bvw1YOeL0Yo.jpg';

export default function Banner({data}) {

  console.log(data);
  const {title, content, destination, label} = data;

  return (
    <Row>
      <Col className="p-5 text-center">
        <img src={imgCoffeeCup} alt="coffee cup" height="250px" />
        <h1>{title}</h1>
        <p>{content}</p>
        <Link to={destination}>{label}</Link>
      </Col>
    </Row>
  )
}
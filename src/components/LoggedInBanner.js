import { Row, Col } from 'react-bootstrap';

export default function LoggedInBanner({firstName}) {

  //console.log(firstName);

  return (
    <Row>
      <Col className="p-2">
        <h3>Hi {firstName}!</h3>
      </Col>
    </Row>
  )
}
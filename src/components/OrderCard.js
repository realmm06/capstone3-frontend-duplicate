import { Card, Col, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import OrderProductCard from '../components/OrderProductCard';
import UserContext from "../UserContext";
import {Link, useNavigate} from 'react-router-dom';
import Swal from "sweetalert2";

export default function OrderCard({orderProp}) {

  const { user } = useContext(UserContext);
  const {_id, userId, status, purchasedOn, totalAmount, products} = orderProp;
  const [product, setProduct] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    setProduct(products.map((product, productId) => {
      return(
        <OrderProductCard key={productId} orderProductProp={product} />
      )
    }));
  }, [])


  // Set order from pending to paid
  const pay = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${id}/pay`, {
      method: "PATCH",
      headers:{
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        orderId: _id,
        status: "paid"
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(data){
        Swal.fire({
          title: "Payment successful",
          icon: "success",
          text: "Order is now paid."
        });

        navigate("/products");
      }
      else{
        Swal.fire({
          title: "Payment error",
          icon: "error",
          text: "Something went wrong. Please try again later!"
        });
      }
    })
  }

  return (
    (user.isAdmin)
    ?
    <Col md={{span: 8, offset: 2}}>
      <Card className="mt-2 mb-2">
        <Card.Body>
          <Card.Title>Order Id: {_id}</Card.Title>
          <Card.Title>User Id: {userId}</Card.Title>
          <Card.Title>Order purchased on: {purchasedOn.slice(0,10)}</Card.Title>
          <Card.Subtitle>Status:</Card.Subtitle>
          <Card.Text>{status}</Card.Text>
          <Card.Subtitle>Total Amount:</Card.Subtitle>
          <Card.Text>Php {totalAmount}</Card.Text>
            {product}
        </Card.Body>
      </Card>
    </Col>
    :
    <Col md={{span: 8, offset: 2}}>
      <Card className="mt-2 mb-2">
        <Card.Body>
          <Card.Title>Order purchased on: {purchasedOn.slice(0,10)}</Card.Title>
          <Card.Subtitle>Status:</Card.Subtitle>
          <Card.Text>{status}</Card.Text>
          <Card.Subtitle>Total Amount:</Card.Subtitle>
          <Card.Text>Php {totalAmount}</Card.Text>

          {(status === "pending")
          ?
          <Card.Text>
            <Button variant="danger" onClick={() => pay(userId)}>
              Pay
            </Button>
          </Card.Text>
          :
          <Card.Text> </Card.Text>
          }

          {product}
        </Card.Body>
      </Card>
    </Col>
  )
}
import { Card, Button, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {

  const {_id, name, description, price, stocks} = productProp;

  return (
    <Col md={{ span: 4}}>
      <Card className="mt-2 mb-2">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{price}</Card.Text>
          <Card.Subtitle>Stocks:</Card.Subtitle>
          <Card.Text>{stocks}</Card.Text>
          <Button as={Link} to={`/products/${_id}`} variant="danger">Details</Button>
        </Card.Body>
      </Card>
    </Col>
  )
}
import './App.css';
import { Row, Col, Container } from 'react-bootstrap';
import {BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AdminDashboard from './pages/AdminDashboard';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Orders from './pages/Orders';
import OrderCard from './components/OrderCard';
import Error from './pages/Error';

import { UserProvider } from './UserContext';
import { useState, useEffect, useContext } from 'react';

function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    id: localStorage.getItem('id'),
    isAdmin: localStorage.getItem('isAdmin'),
    firstName: localStorage.getItem('firstName')
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  // added for appnavbar behavior
  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem('token')}`
  //     }
  //   })
  //   .then(res => res.json())
  //   .then(data => {
  //     //console.log(data);
  //     if(typeof data._id !== undefined){
  //       setUser({
  //         id: data._id,
  //         isAdmin: data.isAdmin,
  //         firstName: data.firstName
  //       })

  //       localStorage.setItem("userId", data._id);
  //       localStorage.setItem("userRole", data.isAdmin);
  //       localStorage.setItem("firstName", data.firstName);
  //     }
  //     else{
  //       setUser({
  //         id: null,
  //         isAdmin: null
  //       });        
  //     }
  //   })
  // }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <Row className="pt-3 pb-3 appHeight">
        <Col className="col-md-1">
        </Col>
        <Col className="col-md-10 col-12">
        
          <Container fluid className="border border-dark rounded">
            <AppNavbar />  
            <Routes>
              <Route path="/" element={<Home />} />
              <Route exact path="/admin" element={<AdminDashboard />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/orders/" element={<Orders />} />
              <Route path="users/:userId/pay" element={<OrderCard />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>

        </Col>
        <Col className="col-md-1">
        </Col>
      </Row>
    </Router>
    </UserProvider>

  );
}

export default App;

import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register(){

	const {user} = useContext(UserContext);

  const navigate = useNavigate();

  // State hooks
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  // State hook - submit button
  const [isActive, setIsActive] = useState(false);

  console.log(firstName);
  console.log(lastName);
  console.log(email);
  console.log(mobileNo);
  console.log(password1);
  console.log(password2);

  function registerUser(event){

    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail/`, {
      method: 'POST',
      headers: { 'Content-type' : 'application/json'},
      body: JSON.stringify({
        email: email
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data){
        Swal.fire({
          title: "Duplicate email found",
          icon: "error",
          text: "Please provide a different email."
        })
      }
      else{
        registerNewUser();
      }
    })
  }

  const registerNewUser = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: 'POST',
      headers: { 'Content-type' : 'application/json'},
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password1,
        mobileNo: mobileNo
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data){
        setFirstName('');
        setLastName('');
        setEmail('');
        setMobileNo('');
        setPassword1('');
        setPassword2('');

        Swal.fire({
          title: "Registration successful",
          icon: "success",
          text: "Please log in."
        })

        navigate("/login")
      } 
      else{
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }
    })
  }

  useEffect(() => {
    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && mobileNo.length >= 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
        setIsActive(true);
    }
    else{
        setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password1, password2])

  return(
    (user.id !== null)
    ?
    <Navigate to ="/products" />
    :
    <Container fluid>
    <h3>Register</h3>
    <Row>
      <Col className="col-md-3">
      </Col>
      <Col className="col-md-6 col-12">
        <Form onSubmit={(event) => registerUser(event)}>

          <Form.Group controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control 
              type="firstName" 
              placeholder="Enter First Name" 
              value = {firstName}
              onChange = {event => setFirstName(event.target.value)}
              required />
          </Form.Group>

          <Form.Group controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control 
              type="lastName" 
              placeholder="Enter Last Name" 
              value = {lastName}
              onChange = {event => setLastName(event.target.value)}
              required />
          </Form.Group>

          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
              type="email" 
              placeholder="Enter email" 
              value = {email}
              onChange = {event => setEmail(event.target.value)}
              required />
          </Form.Group>

          <Form.Group controlId="mobileNo">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control 
              type="mobileNo" 
              placeholder="Enter Mobile Number" 
              value = {mobileNo}
              onChange = {event => setMobileNo(event.target.value)}
              required />
          </Form.Group>

          <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Enter Password" 
              value = {password1}
              onChange = {event => setPassword1(event.target.value)}
              required />
          </Form.Group>

          <Form.Group controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Verify Password" 
              value = {password2}
              onChange = {event => setPassword2(event.target.value)}
              required />
          </Form.Group>

          <div className='pt-3 pb-3'>
            { isActive 
              ?
              <Button variant="danger" type="submit" id="submitBtn">
                Submit
              </Button>
              :
              <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
              </Button>
            }
          </div>
        </Form>
      </Col>
      <Col className="col-md-3">
      </Col>
    </Row>
    </Container>
  )
}
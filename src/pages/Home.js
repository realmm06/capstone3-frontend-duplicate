import { Fragment } from 'react';
import Banner from '../components/Banner';

export default function Home() {
	const data = {
		title: "<Coffee Break>",
		content: "Buy coffee in an instant.",
		destination: '/products',
		label: "Buy now!"
	}

	return (
		<Fragment>
			<Banner data={data} />
		</Fragment>
	)
}
